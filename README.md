# Revolut Backend Test

This project is implemented according to the [assignment document](Backend Home Task 2019-05.pdf) with Java 11 for a RESTful API to handle money transfers between accounts.

## Build and run

Java 11 and Maven are required for below commands.

```bash
# build
mvn -Dmaven.test.skip=true package

# run the standalone jar
java -jar target/jetty-embedded-example-1.0-SNAPSHOT.jar
```
  
## Test

Tests are into 3 categories: **Unit Test** has the name pattern like ``*Test``, **Integration Test** like ``*IT``, and **Load Test** like ``*LT``,

```bash
# run Unit Tests and Integration Tests
mvn -Dtest=*Test,*IT test

# run the Load Tests
mvn -Dtest=*LT test
```

## RESTful API Endpoint

### Account resource
```bash
# Create an account with given balance
curl -d "currency=EUR&balance=100" -X POST http://localhost:8080/api/accounts

# Get account by id
curl http://localhost:8080/api/accounts/1
```

### Transaction resource 
```bash
# Create a transfer transaction between two accounts
curl -d "fromAccountId=1&toAccountId=2&currency=EUR&amount=10" -X POST http://localhost:8080/api/transactions

# Get transaction by id
curl http://localhost:8080/api/transactions/1
```

## Libraries used
* Jersey - a REST framework
* Jetty - a Web server and javax.servlet container
* Guice 4.2.2 - a lightweight dependency injection framework
* jOOQ 3.12.1 - a fluent API for typesafe SQL query construction and execution
* log4j-slf4j-impl 2.12.1 - logging tool
* BeanUtils 1.9.3 - to copy attributes between two different class objects
* HikariCP 3.4.1 - a high-performance JDBC connection pool
* H2 1.4.199 - a fast embedded Java SQL database
* Flyway 6.0.4 - DB Migration tool for both testing or initializing the application
* JUnit 4.12 - a unit testing framework
* JSONAssert 1.5.0 - Used in tests for JSON string assertion
