package com.revolut.assignment.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Transaction {
  private long id;
  private long fromAccountId;
  private long toAccountId;
  private BigDecimal amount;
  private String currency;
  private LocalDateTime createdDT;

  public Transaction() {}

  public Transaction(long fromAccountId, long toAccountId, BigDecimal amount, String currency) {
    this.fromAccountId = fromAccountId;
    this.toAccountId = toAccountId;
    this.amount = amount;
    this.currency = currency;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getFromAccountId() {
    return fromAccountId;
  }

  public void setFromAccountId(long fromAccountId) {
    this.fromAccountId = fromAccountId;
  }

  public long getToAccountId() {
    return toAccountId;
  }

  public void setToAccountId(long toAccountId) {
    this.toAccountId = toAccountId;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public LocalDateTime getCreatedDT() {
    return createdDT;
  }

  public void setCreatedDT(LocalDateTime createdDT) {
    this.createdDT = createdDT;
  }
}
