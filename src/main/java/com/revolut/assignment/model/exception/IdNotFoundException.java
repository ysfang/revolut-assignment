package com.revolut.assignment.model.exception;

public class IdNotFoundException extends RuntimeException {
  private String message;

  public IdNotFoundException(String resourceType, long id) {
    this.message = String.format("%s: %d is not found", resourceType, id);
  }

  public String getMessage() {
    return message;
  }
}
