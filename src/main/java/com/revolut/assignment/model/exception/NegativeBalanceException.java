package com.revolut.assignment.model.exception;

public class NegativeBalanceException extends RuntimeException {
  private String message = "account balance cannot be negative";

  public String getMessage() {
    return message;
  }
}
