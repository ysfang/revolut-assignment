package com.revolut.assignment.model.exception;

import java.math.BigDecimal;

public class InvalidOperationException extends RuntimeException {
  private String message;

  public InvalidOperationException(
      String operation, long accountId, BigDecimal amount, String currency) {
    this.message =
        String.format(
            "Invalid %s operation for Account: %d, amount: %s, currency: %s",
            operation, accountId, amount.toString(), currency);
  }

  public String getMessage() {
    return message;
  }
}
