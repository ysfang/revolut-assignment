package com.revolut.assignment.model.exception;

public class CurrencyNotSupportException extends RuntimeException {
  private String message;

  public CurrencyNotSupportException(String currency) {
    this.message = String.format("Not supported with given Currency: %s", currency);
  }

  public String getMessage() {
    return message;
  }
}
