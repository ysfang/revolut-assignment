package com.revolut.assignment.util;

import com.revolut.assignment.config.Var;
import com.revolut.assignment.service.impl.DBServiceImpl;
import org.jooq.codegen.GenerationTool;
import org.jooq.meta.h2.H2Database;
import org.jooq.meta.jaxb.Configuration;
import org.jooq.meta.jaxb.Database;
import org.jooq.meta.jaxb.Generator;
import org.jooq.meta.jaxb.Target;

public class JooqCodeGenUtils {
  private static DBServiceImpl dbServiceImpl = new DBServiceImpl(Var.DB_PROP_PATH);

  public static void main(String[] args) throws Exception {

    Configuration configuration = new Configuration();
    configuration
        .withJdbc(dbServiceImpl.getJdbc())
        .withGenerator(
            new Generator()
                .withDatabase(
                    new Database()
                        .withName(H2Database.class.getName())
                        .withInputSchema("PUBLIC")
                        .withExcludes("INFORMATION_SCHEMA.*|FLYWAY.*"))
                .withTarget(
                    new Target()
                        .withPackageName("com.revolut.assignment.model.generated")
                        .withDirectory("./src/main/java")));
    (new GenerationTool()).run(configuration);
  }
}
