package com.revolut.assignment.util;

import com.revolut.assignment.config.Var;
import com.revolut.assignment.service.impl.DBServiceImpl;
import org.flywaydb.core.Flyway;

import java.util.Properties;

public class FlywayUtils {
  private static DBServiceImpl dbServiceImpl = new DBServiceImpl(Var.DB_PROP_PATH);

  public static void migrateDB() {
    Properties properties = dbServiceImpl.getProperties();
    Flyway flyway =
        Flyway.configure()
            .dataSource(
                properties.getProperty("jdbc.url"),
                properties.getProperty("jdbc.username"),
                properties.getProperty("jdbc.password"))
            .load();
    flyway.migrate();
  }

  public static void cleanDB() {
    Properties properties = dbServiceImpl.getProperties();
    Flyway flyway =
        Flyway.configure()
            .dataSource(
                properties.getProperty("jdbc.url"),
                properties.getProperty("jdbc.username"),
                properties.getProperty("jdbc.password"))
            .load();
    flyway.clean();
  }

  public static void cleanAndMigrate() {
    cleanDB();
    migrateDB();
  }
}
