package com.revolut.assignment.util;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.revolut.assignment.config.AppModule;

public class GuiceInjectorUtils {
  private static Injector injector = Guice.createInjector(new AppModule());

  public static Injector getInjector() {
    return injector;
  }
}
