package com.revolut.assignment.controller;

import com.revolut.assignment.controller.response.ExceptionResponse;
import com.revolut.assignment.model.exception.CurrencyNotSupportException;
import com.revolut.assignment.model.exception.IdNotFoundException;
import com.revolut.assignment.model.exception.InvalidOperationException;
import com.revolut.assignment.model.exception.NegativeBalanceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static javax.ws.rs.core.Response.status;

@Provider
public class GeneralExceptionMapper implements ExceptionMapper<Exception> {
  private static final Logger logger = LoggerFactory.getLogger(GeneralExceptionMapper.class);

  @Override
  public Response toResponse(Exception e) {
    Response.Status status = Response.Status.INTERNAL_SERVER_ERROR;
    String message = e.getMessage();
    if (e instanceof IdNotFoundException) {
      status = Response.Status.BAD_REQUEST;
    } else if (e instanceof CurrencyNotSupportException) {
      status = Response.Status.BAD_REQUEST;
    } else if (e instanceof InvalidOperationException) {
      status = Response.Status.BAD_REQUEST;
    } else if (e instanceof NegativeBalanceException) {
      status = Response.Status.BAD_REQUEST;
    } else {
      e.printStackTrace();
    }

    return status(status)
        .entity(new ExceptionResponse(status.getStatusCode(), message))
        .type(MediaType.APPLICATION_JSON_TYPE)
        .build();
  }
}
