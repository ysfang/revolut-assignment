package com.revolut.assignment.controller;

import com.revolut.assignment.model.Transaction;
import com.revolut.assignment.service.TransactionService;
import com.revolut.assignment.util.GuiceInjectorUtils;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

@Path("/transactions")
public class TransactionController {
  private final TransactionService transactionService;

  public TransactionController() {
    this.transactionService =
        GuiceInjectorUtils.getInjector().getInstance(TransactionService.class);
  }

  @GET
  @Path("{id: [0-9]+}")
  @Produces(MediaType.APPLICATION_JSON)
  public Transaction getTransactionById(@PathParam("id") long id)
      throws InvocationTargetException, IllegalAccessException {
    return transactionService.getTransaction(id);
  }

  @POST
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(MediaType.APPLICATION_JSON)
  public Transaction createTransaction(
      @NotNull(message = "transfer from the given account") @FormParam("fromAccountId")
          Long fromAccountId,
      @NotNull(message = "transfer to the given account") @FormParam("toAccountId")
          Long toAccountId,
      @NotNull(message = "currency code (EUR available currently)") @FormParam("currency")
          String currency,
      @NotNull(message = "transfer amount") @FormParam("amount") BigDecimal amount)
      throws Exception {
    return transactionService.createTransaction(fromAccountId, toAccountId, amount, currency);
  }
}
