package com.revolut.assignment.controller;

import com.revolut.assignment.model.Account;
import com.revolut.assignment.service.AccountService;
import com.revolut.assignment.util.GuiceInjectorUtils;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

@Path("/accounts")
public class AccountController {
  private final AccountService accountService;

  public AccountController() {
    this.accountService = GuiceInjectorUtils.getInjector().getInstance(AccountService.class);
  }

  @GET
  @Path("{id: [0-9]+}")
  @Produces(MediaType.APPLICATION_JSON)
  public Account getAccountById(@PathParam("id") long id)
      throws InvocationTargetException, IllegalAccessException {
    return accountService.getAccount(id);
  }

  @POST
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(MediaType.APPLICATION_JSON)
  public Account createAccount(
      @NotNull(message = "currency code (EUR available currently)") @FormParam("currency")
          String currency,
      @NotNull(message = "create account with given balance") @FormParam("balance")
          BigDecimal balance)
      throws InvocationTargetException, IllegalAccessException {
    return accountService.createAccount(currency, balance);
  }
}
