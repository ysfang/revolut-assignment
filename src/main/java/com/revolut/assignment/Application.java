package com.revolut.assignment;

import com.revolut.assignment.util.FlywayUtils;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;

import static com.revolut.assignment.config.Var.BASE_URI;

public class Application {
  private static final Logger logger = LoggerFactory.getLogger(Application.class);

  public static void main(String[] args) throws IOException {
    Application application = new Application();
    final HttpServer server = application.start();
    logger.info("Initializing Database...");
    FlywayUtils.cleanAndMigrate();

    logger.info(String.format("Application is running on base uri: %s", BASE_URI));
    logger.info("Hit enter to stop it...");
    System.in.read();
    server.stop();
  }

  public HttpServer start() {
    final ResourceConfig rc = new ResourceConfig().packages("com.revolut.assignment.controller");
    return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
  }
}
