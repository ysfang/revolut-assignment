package com.revolut.assignment.config;

public class Var {
  public static final String BASE_URI = "http://localhost:8080/api/";

  public static final String DB_PROP_PATH = "db/db.properties";

  public static final int TIMEOUT_FOR_WAITING_TRANS_LOCKS = 3000;
}
