package com.revolut.assignment.config;

import com.google.inject.AbstractModule;
import com.revolut.assignment.service.AccountService;
import com.revolut.assignment.service.DBService;
import com.revolut.assignment.service.LockService;
import com.revolut.assignment.service.TransactionService;
import com.revolut.assignment.service.impl.AccountServiceImpl;
import com.revolut.assignment.service.impl.DBServiceImpl;
import com.revolut.assignment.service.impl.LockServiceImpl;
import com.revolut.assignment.service.impl.TransactionServiceImpl;

public class AppModule extends AbstractModule {
  @Override
  protected void configure() {
    bind(AccountService.class).to(AccountServiceImpl.class);
    bind(TransactionService.class).to(TransactionServiceImpl.class);
    bind(LockService.class).to(LockServiceImpl.class);
    bind(DBService.class).toInstance(new DBServiceImpl(Var.DB_PROP_PATH));
  }
}
