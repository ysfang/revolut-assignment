package com.revolut.assignment.service;

public interface LockService {
  void lockAccountsForOperation(long fromAccountId, long toAccountId, Runnable runnable)
      throws Exception;

  boolean isAccountLocked(long accountId);
}
