package com.revolut.assignment.service;

import org.jooq.DSLContext;
import org.jooq.meta.jaxb.Jdbc;

import java.util.Properties;

public interface DBService {
  Properties getProperties();

  Jdbc getJdbc();

  DSLContext getDSLContext();
}
