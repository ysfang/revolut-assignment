package com.revolut.assignment.service;

import com.revolut.assignment.model.Account;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

public interface AccountService {
  Account createAccount(String currency, BigDecimal balance)
      throws InvocationTargetException, IllegalAccessException;

  Account getAccount(long id) throws InvocationTargetException, IllegalAccessException;

  int deposit(long accountId, BigDecimal amount, String currency);

  int withdraw(long accountId, BigDecimal amount, String currency);
}
