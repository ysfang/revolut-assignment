package com.revolut.assignment.service;

import com.revolut.assignment.model.Transaction;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

public interface TransactionService {
  Transaction getTransaction(long id) throws InvocationTargetException, IllegalAccessException;

  Transaction createTransaction(
      long fromAccountId, long toAccountId, BigDecimal amount, String currency) throws Exception;
}
