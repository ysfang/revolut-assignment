package com.revolut.assignment.service.impl;

import com.google.inject.Singleton;
import com.revolut.assignment.service.LockService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.ReentrantLock;

import static com.revolut.assignment.config.Var.TIMEOUT_FOR_WAITING_TRANS_LOCKS;

@Singleton
public class LockServiceImpl implements LockService {
  private final Map<Long, ReentrantLock> accountMap = new ConcurrentHashMap();

  @Override
  public void lockAccountsForOperation(long fromAccountId, long toAccountId, Runnable runnable)
      throws Exception {
    accountMap.putIfAbsent(fromAccountId, new ReentrantLock());
    accountMap.putIfAbsent(toAccountId, new ReentrantLock());

    ReentrantLock fromAccountLock = accountMap.get(fromAccountId);
    ReentrantLock toAccountLock = accountMap.get(toAccountId);

    boolean isReady = false;
    long millisToTimeout = System.currentTimeMillis() + TIMEOUT_FOR_WAITING_TRANS_LOCKS;

    do {
      if (System.currentTimeMillis() > millisToTimeout) {
        throw new TimeoutException("timeout for fetching transaction locks");
      }
      if (fromAccountLock.tryLock()) {
        if (toAccountLock.tryLock()) {
          isReady = true;
        } else {
          fromAccountLock.unlock();
        }
      }
    } while (!isReady);

    try {
      runnable.run();
    } finally {
      toAccountLock.unlock();
      fromAccountLock.unlock();
    }
  }

  @Override
  public boolean isAccountLocked(long accountId) {
    return accountMap.get(accountId).isLocked();
  }
}
