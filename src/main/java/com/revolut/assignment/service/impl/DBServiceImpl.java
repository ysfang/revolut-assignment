package com.revolut.assignment.service.impl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.assignment.service.DBService;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.meta.jaxb.Jdbc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Singleton
public class DBServiceImpl implements DBService {
  private static final Logger logger = LoggerFactory.getLogger(DBService.class);

  private final DSLContext dslContext;
  private Properties properties = new Properties();

  @Inject
  public DBServiceImpl(String dbPropPath) {
    logger.trace(String.format("loading db properties from: %s", dbPropPath));
    try (InputStream inputStream =
        Thread.currentThread().getContextClassLoader().getResourceAsStream(dbPropPath)) {
      properties.load(inputStream);
    } catch (IOException e) {
      throw new RuntimeException("Error to load DB properties", e);
    }

    HikariConfig config = new HikariConfig();
    config.setJdbcUrl(properties.getProperty("jdbc.url"));
    config.setUsername(properties.getProperty("jdbc.username"));
    config.setPassword(properties.getProperty("jdbc.password"));
    DataSource dataSource = new HikariDataSource(config);
    dslContext = DSL.using(dataSource, SQLDialect.H2);
  }

  public Properties getProperties() {
    return properties;
  }

  public Jdbc getJdbc() {
    return new Jdbc()
        .withDriver(properties.getProperty("jdbc.driver"))
        .withUrl(properties.getProperty("jdbc.url"))
        .withUsername(properties.getProperty("jdbc.username"))
        .withPassword(properties.getProperty("jdbc.password"));
  }

  public DSLContext getDSLContext() {
    return dslContext;
  }
}
