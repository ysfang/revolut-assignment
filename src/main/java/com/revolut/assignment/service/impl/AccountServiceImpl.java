package com.revolut.assignment.service.impl;

import com.google.inject.Singleton;
import com.revolut.assignment.model.Account;
import com.revolut.assignment.model.Currency;
import com.revolut.assignment.model.exception.CurrencyNotSupportException;
import com.revolut.assignment.model.exception.IdNotFoundException;
import com.revolut.assignment.model.exception.InvalidOperationException;
import com.revolut.assignment.model.exception.NegativeBalanceException;
import com.revolut.assignment.model.generated.tables.records.AccountRecord;
import com.revolut.assignment.service.AccountService;
import com.revolut.assignment.service.DBService;
import org.apache.commons.beanutils.BeanUtils;

import javax.inject.Inject;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Arrays;

import static com.revolut.assignment.model.generated.Tables.ACCOUNT;

@Singleton
public class AccountServiceImpl implements AccountService {
  @Inject private DBService dbService;

  public AccountServiceImpl() {}

  public AccountServiceImpl(DBService dbService) {
    this.dbService = dbService;
  }

  @Override
  public Account createAccount(String currency, BigDecimal amount)
      throws InvocationTargetException, IllegalAccessException {
    Account account;

    validateAccountCreation(currency, amount);
    AccountRecord accountRecord =
        dbService
            .getDSLContext()
            .insertInto(ACCOUNT, ACCOUNT.CURRENCY, ACCOUNT.BALANCE)
            .values(currency, amount)
            .returning()
            .fetchOne();
    account = new Account();
    BeanUtils.copyProperties(account, accountRecord);

    return account;
  }

  @Override
  public Account getAccount(long id) throws InvocationTargetException, IllegalAccessException {
    AccountRecord accountRecord = getAccountRecord(id);
    Account account = new Account();
    BeanUtils.copyProperties(account, accountRecord);

    return account;
  }

  @Override
  public int deposit(long accountId, BigDecimal amount, String currency) {
    int result =
        dbService
            .getDSLContext()
            .update(ACCOUNT)
            .set(ACCOUNT.BALANCE, ACCOUNT.BALANCE.add(amount))
            .where(ACCOUNT.ID.eq(accountId))
            .and(ACCOUNT.CURRENCY.eq(currency))
            .execute();
    if (result == 0) {
      throw new InvalidOperationException("deposit", accountId, amount, currency);
    }
    return result;
  }

  @Override
  public int withdraw(long accountId, BigDecimal amount, String currency) {
    int result =
        dbService
            .getDSLContext()
            .update(ACCOUNT)
            .set(ACCOUNT.BALANCE, ACCOUNT.BALANCE.subtract(amount))
            .where(ACCOUNT.ID.eq(accountId))
            .and(ACCOUNT.BALANCE.ge(amount))
            .and(ACCOUNT.CURRENCY.eq(currency))
            .execute();
    if (result == 0) {
      throw new InvalidOperationException("withdraw", accountId, amount, currency);
    }
    return result;
  }

  private AccountRecord getAccountRecord(long id) {
    AccountRecord accountRecord =
        dbService.getDSLContext().selectFrom(ACCOUNT).where(ACCOUNT.ID.eq(id)).fetchOne();
    if (accountRecord == null) {
      throw new IdNotFoundException("Account", id);
    }

    return accountRecord;
  }

  private void validateAccountCreation(String currency, BigDecimal amount) {
    if (Arrays.stream(Currency.values())
        .filter(cu -> cu.name().equals(currency))
        .findAny()
        .isEmpty()) {
      throw new CurrencyNotSupportException(currency);
    }
    if (amount.compareTo(BigDecimal.ZERO) < 0) {
      throw new NegativeBalanceException();
    }
  }
}
