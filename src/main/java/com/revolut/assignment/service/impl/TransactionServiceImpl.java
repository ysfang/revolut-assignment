package com.revolut.assignment.service.impl;

import com.google.inject.Singleton;
import com.revolut.assignment.model.Transaction;
import com.revolut.assignment.model.exception.IdNotFoundException;
import com.revolut.assignment.model.generated.tables.records.TransactionRecord;
import com.revolut.assignment.service.AccountService;
import com.revolut.assignment.service.DBService;
import com.revolut.assignment.service.LockService;
import com.revolut.assignment.service.TransactionService;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

import static com.revolut.assignment.model.generated.Tables.TRANSACTION;

@Singleton
public class TransactionServiceImpl implements TransactionService {
  private static final Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);

  private DBService dbService;
  private AccountService accountService;
  private LockService lockService;

  @Inject
  public TransactionServiceImpl(
      DBService dbService, LockService lockService, AccountService accountService) {
    this.dbService = dbService;
    this.lockService = lockService;
    this.accountService = accountService;
  }

  @Override
  public Transaction getTransaction(long id)
      throws InvocationTargetException, IllegalAccessException {
    TransactionRecord transactionRecord = getAccountRecord(id);
    Transaction transaction = new Transaction();
    BeanUtils.copyProperties(transaction, transactionRecord);
    transaction.setCreatedDT(transactionRecord.getCreatedTs().toLocalDateTime());

    return transaction;
  }

  private TransactionRecord getAccountRecord(long id) {
    TransactionRecord transactionRecord =
        dbService.getDSLContext().selectFrom(TRANSACTION).where(TRANSACTION.ID.eq(id)).fetchOne();
    if (transactionRecord == null) {
      throw new IdNotFoundException("Transaction", id);
    }

    return transactionRecord;
  }

  @Override
  public Transaction createTransaction(
      long fromAccountId, long toAccountId, BigDecimal amount, String currency) throws Exception {
    Transaction transaction = new Transaction();
    lockService.lockAccountsForOperation(
        fromAccountId,
        toAccountId,
        () -> {
          accountService.withdraw(fromAccountId, amount, currency);
          accountService.deposit(toAccountId, amount, currency);

          TransactionRecord transactionRecord =
              dbService
                  .getDSLContext()
                  .insertInto(
                      TRANSACTION,
                      TRANSACTION.FROM_ACCOUNT_ID,
                      TRANSACTION.TO_ACCOUNT_ID,
                      TRANSACTION.AMOUNT,
                      TRANSACTION.CURRENCY)
                  .values(fromAccountId, toAccountId, amount, currency)
                  .returning()
                  .fetchOne();

          try {
            BeanUtils.copyProperties(transaction, transactionRecord);
          } catch (Exception e) {
            logger.error(e.getMessage());
          }
        });

    return transaction;
  }
}
