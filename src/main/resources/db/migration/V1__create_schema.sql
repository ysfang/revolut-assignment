CREATE TABLE account(
	id BIGINT AUTO_INCREMENT PRIMARY KEY,
	currency VARCHAR(10) NOT NULL,
	balance DECIMAL(20,2) NOT NULL
);

CREATE TABLE transaction(
	id BIGINT AUTO_INCREMENT PRIMARY KEY,
	from_account_id BIGINT NOT NULL,
	to_account_id BIGINT NOT NULL,
	currency VARCHAR(10) NOT NULL,
	amount DECIMAL(20,2) NOT NULL,
	created_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP()
);

ALTER TABLE transaction ADD FOREIGN KEY (from_account_id) REFERENCES account(id);
ALTER TABLE transaction ADD FOREIGN KEY (to_account_id) REFERENCES account(id);
