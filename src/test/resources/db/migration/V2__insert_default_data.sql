INSERT INTO account (id, currency, balance)
VALUES (1, 'EUR', 100.00),
       (2, 'EUR', 200.00),
       (3, 'EUR', 300.00),
       (4, 'EUR', 400.00),
       (5, 'EUR', 500.00);

INSERT INTO transaction (id, from_account_id, to_account_id, currency, amount, created_ts) VALUES (1, 1, 2, 'EUR', 100.00, '2019-09-29 00:10:00.000000000');
