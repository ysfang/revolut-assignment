package com.revolut.assignment.service;

import com.revolut.assignment.config.Var;
import com.revolut.assignment.model.Account;
import com.revolut.assignment.model.Transaction;
import com.revolut.assignment.model.exception.InvalidOperationException;
import com.revolut.assignment.service.impl.AccountServiceImpl;
import com.revolut.assignment.service.impl.DBServiceImpl;
import com.revolut.assignment.service.impl.LockServiceImpl;
import com.revolut.assignment.service.impl.TransactionServiceImpl;
import com.revolut.assignment.util.FlywayUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class TransactionServiceTest {
  private static final Logger logger = LoggerFactory.getLogger(TransactionServiceTest.class);

  private DBService dbService = new DBServiceImpl(Var.DB_PROP_PATH);
  private LockService lockService = new LockServiceImpl();
  private AccountService accountService = new AccountServiceImpl(dbService);
  private TransactionService transactionService =
      new TransactionServiceImpl(dbService, lockService, accountService);

  @BeforeClass
  public static void setUpBeforeClass() {
    FlywayUtils.cleanAndMigrate();
  }

  @Test
  public void transfer_whenArgValid_thenReturnTransaction() throws Exception {
    long fromAccountId = 4L;
    long toAccountId = 5L;

    Transaction transaction =
        transactionService.createTransaction(
            fromAccountId, toAccountId, BigDecimal.valueOf(1), "EUR");
    Account fromAccount = getAccountHelper(fromAccountId);
    Account toAccount = getAccountHelper(toAccountId);

    assertEquals(fromAccountId, transaction.getFromAccountId());
    assertEquals(toAccountId, transaction.getToAccountId());
    assertEquals("1.00", transaction.getAmount().toString());
    assertEquals("EUR", transaction.getCurrency());

    assertEquals("399.00", fromAccount.getBalance().toString());
    assertEquals("501.00", toAccount.getBalance().toString());
  }

  @Test(expected = InvalidOperationException.class)
  public void transfer_whenInsufficientFund_thenThrow() throws Exception {
    transactionService.createTransaction(1L, 2L, BigDecimal.valueOf(101), "EUR");
  }

  @Test(expected = InvalidOperationException.class)
  public void transfer_whenIncorrectCurrency_thenThrow() throws Exception {
    transactionService.createTransaction(1L, 2L, BigDecimal.valueOf(1), "USD");
  }

  private Account getAccountHelper(long id)
      throws InvocationTargetException, IllegalAccessException {
    return accountService.getAccount(id);
  }
}
