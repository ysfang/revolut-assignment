package com.revolut.assignment.service;

import com.revolut.assignment.service.impl.LockServiceImpl;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LockServiceTest {
  private static final Logger logger = LoggerFactory.getLogger(LockServiceTest.class);

  private LockService lockService = new LockServiceImpl();

  @Test
  public void lockAccountsForOperation_WhenLockedByAnotherThread_ThenBothAccountsAreUnavailable()
      throws Exception {
    ExecutorService executorService = Executors.newFixedThreadPool(1);

    executorService.submit(
        () -> {
          try {
            lockService.lockAccountsForOperation(
                1L,
                2L,
                () -> {
                  while (true) {}
                });
          } catch (Exception e) {
            e.printStackTrace();
          }
        });
    executorService.shutdown();
    Thread.sleep(500);

    assertTrue(lockService.isAccountLocked(1L));
    assertTrue(lockService.isAccountLocked(2L));
  }

  @Test
  public void
      lockAccountsForOperation_WhenLockReleaseByAnotherThread_ThenBothAccountsBecomeAvailable()
          throws Exception {
    ExecutorService executorService = Executors.newFixedThreadPool(1);

    executorService.submit(
        () -> {
          try {
            lockService.lockAccountsForOperation(
                1L,
                2L,
                () -> {
                  try {
                    Thread.sleep(500);
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }
                });
          } catch (Exception e) {
            e.printStackTrace();
          }
        });
    executorService.shutdown();
    Thread.sleep(1000);

    assertFalse(lockService.isAccountLocked(1L));
    assertFalse(lockService.isAccountLocked(2L));
  }
}
