package com.revolut.assignment.service;

import com.revolut.assignment.config.Var;
import com.revolut.assignment.model.Account;
import com.revolut.assignment.model.Currency;
import com.revolut.assignment.model.exception.CurrencyNotSupportException;
import com.revolut.assignment.model.exception.IdNotFoundException;
import com.revolut.assignment.model.exception.InvalidOperationException;
import com.revolut.assignment.model.exception.NegativeBalanceException;
import com.revolut.assignment.service.impl.AccountServiceImpl;
import com.revolut.assignment.service.impl.DBServiceImpl;
import com.revolut.assignment.util.FlywayUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class AccountServiceTest {
  private DBService dbService = new DBServiceImpl(Var.DB_PROP_PATH);
  private AccountService accountService = new AccountServiceImpl(dbService);

  @BeforeClass
  public static void setUpBeforeClass() {
    FlywayUtils.cleanAndMigrate();
  }

  @Test
  public void getAccount_whenAccountExists_thenReturnAccount()
      throws InvocationTargetException, IllegalAccessException {
    Account account = accountService.getAccount(1L);
    assertEquals("100.00", account.getBalance().toString());
    assertEquals(Currency.EUR.name(), account.getCurrency());
  }

  @Test(expected = IdNotFoundException.class)
  public void getAccount_whenAccountNotExists_thenThrow()
      throws InvocationTargetException, IllegalAccessException {
    accountService.getAccount(100L);
  }

  @Test
  public void createAccount_whenArgValid_thenSaveSuccessfully()
      throws InvocationTargetException, IllegalAccessException {
    Account accountDefault = accountService.createAccount(Currency.EUR.name(), BigDecimal.ZERO);
    Account accountWithGivenBalance =
        accountService.createAccount(Currency.EUR.name(), BigDecimal.valueOf(50));

    assertEquals("0.00", accountDefault.getBalance().toString());
    assertEquals(Currency.EUR.name(), accountDefault.getCurrency());
    assertEquals("50.00", accountWithGivenBalance.getBalance().toString());
    assertEquals(Currency.EUR.name(), accountWithGivenBalance.getCurrency());
  }

  @Test(expected = CurrencyNotSupportException.class)
  public void createAccount_whenCurrencyNotSupported_thenThrow()
      throws InvocationTargetException, IllegalAccessException {
    accountService.createAccount("USD", BigDecimal.valueOf(50));
  }

  @Test(expected = NegativeBalanceException.class)
  public void createAccount_whenAmountNegative_thenThrow()
      throws InvocationTargetException, IllegalAccessException {
    accountService.createAccount(Currency.EUR.name(), BigDecimal.valueOf(-1));
  }

  @Test(expected = InvalidOperationException.class)
  public void deposit_whenCurrencyInvalid_thenThrow() {
    accountService.deposit(2L, BigDecimal.valueOf(100), "USD");
  }

  @Test
  public void deposit_whenArgValid_thenSaveSuccessfully()
      throws InvocationTargetException, IllegalAccessException {
    int result = accountService.deposit(2L, BigDecimal.valueOf(1), "EUR");
    Account account = accountService.getAccount(2L);

    assertEquals(1, result);
    assertEquals("201.00", account.getBalance().toString());
  }

  @Test
  public void withdraw_whenArgValid_thenSaveSuccessfully()
      throws InvocationTargetException, IllegalAccessException {
    int result = accountService.withdraw(3L, BigDecimal.valueOf(1), "EUR");
    Account account = accountService.getAccount(3L);

    assertEquals(1, result);
    assertEquals("299.00", account.getBalance().toString());
  }

  @Test(expected = InvalidOperationException.class)
  public void withdraw_whenBalanceNotEnough_thenThrow()
      throws InvocationTargetException, IllegalAccessException {
    accountService.withdraw(3L, BigDecimal.valueOf(301), "EUR");
  }
}
