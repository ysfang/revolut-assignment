package com.revolut.assignment.e2e;

import com.revolut.assignment.Application;
import com.revolut.assignment.model.Currency;
import com.revolut.assignment.util.FlywayUtils;
import org.glassfish.grizzly.http.server.HttpServer;
import org.jooq.tools.json.JSONObject;
import org.json.JSONException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import javax.ws.rs.client.*;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

import static com.revolut.assignment.config.Var.BASE_URI;

public class AccountIT {
  private static HttpServer server;
  private static WebTarget target;

  @BeforeClass
  public static void setUpBeforeClass() {
    FlywayUtils.cleanAndMigrate();
    Application application = new Application();
    server = application.start();
    Client client = ClientBuilder.newClient();
    target = client.target(BASE_URI);
  }

  @AfterClass
  public static void tearDown() throws Exception {
    server.stop();
  }

  @Test
  public void getAccount_whenFound_thenReturnAccountData() throws JSONException {
    Invocation.Builder builder = target.path("accounts/1").request(MediaType.APPLICATION_JSON);
    Response response = builder.get(Response.class);
    String body = response.readEntity(String.class);
    JSONObject expected = new JSONObject();
    expected.put("id", 1l);
    expected.put("currency", Currency.EUR.name());
    expected.put("balance", BigDecimal.valueOf(100.00));

    JSONAssert.assertEquals(expected.toString(), body, true);
  }

  @Test
  public void getAccount_whenNotFound_thenReturnExceptionMessage() throws JSONException {
    Invocation.Builder builder = target.path("accounts/10").request(MediaType.APPLICATION_JSON);
    Response response = builder.get(Response.class);
    String body = response.readEntity(String.class);
    JSONObject expected = new JSONObject();
    expected.put("message", "Account: 10 is not found");
    expected.put("statusCode", 400);

    JSONAssert.assertEquals(expected.toString(), body, true);
  }

  @Test
  public void createAccount_whenArgValid_thenReturnAccountData() throws JSONException {
    Invocation.Builder builder = target.path("accounts").request(MediaType.APPLICATION_JSON);
    Form form = new Form().param("currency", "EUR").param("balance", "100");
    Response response =
        builder.post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED), Response.class);
    String body = response.readEntity(String.class);
    JSONObject expected = new JSONObject();
    expected.put("currency", Currency.EUR.name());
    expected.put("balance", BigDecimal.valueOf(100.00));

    JSONAssert.assertEquals(expected.toString(), body, false);
  }
}
