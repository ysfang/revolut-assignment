package com.revolut.assignment.e2e;

import com.revolut.assignment.Application;
import com.revolut.assignment.model.Currency;
import com.revolut.assignment.util.FlywayUtils;
import org.glassfish.grizzly.http.server.HttpServer;
import org.jooq.tools.json.JSONObject;
import org.json.JSONException;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.*;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.revolut.assignment.config.Var.BASE_URI;

public class TransactionLT {
  private static final Logger logger = LoggerFactory.getLogger(TransactionLT.class);

  private static HttpServer server;
  private static WebTarget target;

  @BeforeClass
  public static void setUpBeforeClass() {
    Application application = new Application();
    server = application.start();
    Client client = ClientBuilder.newClient();
    target = client.target(BASE_URI);
  }

  @AfterClass
  public static void tearDown() {
    server.stop();
  }

  @Before
  public void setUp() {
    FlywayUtils.cleanAndMigrate();
  }

  @Test
  public void transfer_oneWayWithMultiThreads_thenHaveCorrectResult()
      throws InterruptedException, JSONException {
    int totalTimes = 1000;
    long fromAccountId = 1L;
    long toAccountId = 2L;
    ExecutorService executorService =
        Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    CountDownLatch countDownLatch = new CountDownLatch(totalTimes);

    for (int i = 0; i < totalTimes; i++) {
      executorService.submit(
          () -> {
            Invocation.Builder builder =
                target.path("transactions").request(MediaType.APPLICATION_JSON);
            Form form =
                new Form()
                    .param("fromAccountId", "1")
                    .param("toAccountId", "2")
                    .param("currency", "EUR")
                    .param("amount", "0.01");
            builder.post(
                Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED), Response.class);
            countDownLatch.countDown();
          });
    }
    executorService.shutdown();
    countDownLatch.await();

    JSONObject fromAccountExpected = new JSONObject();
    fromAccountExpected.put("id", fromAccountId);
    fromAccountExpected.put("currency", Currency.EUR.name());
    fromAccountExpected.put("balance", BigDecimal.valueOf(90.00));
    JSONObject toAccountExpected = new JSONObject();
    toAccountExpected.put("id", toAccountId);
    toAccountExpected.put("currency", Currency.EUR.name());
    toAccountExpected.put("balance", BigDecimal.valueOf(210.00));

    JSONAssert.assertEquals(fromAccountExpected.toString(), getAccountBalance(fromAccountId), true);
    JSONAssert.assertEquals(toAccountExpected.toString(), getAccountBalance(toAccountId), true);
  }

  @Test
  public void transfer_toEachOtherInRandomOrderWithMultiThreads_thenHaveCorrectResult()
      throws InterruptedException, JSONException {

    List<Form> formList = new ArrayList();
    for (int i = 0; i < 500; i++) {
      formList.add(
          new Form()
              .param("fromAccountId", "1")
              .param("toAccountId", "2")
              .param("currency", "EUR")
              .param("amount", "0.01"));
      formList.add(
          new Form()
              .param("fromAccountId", "2")
              .param("toAccountId", "1")
              .param("currency", "EUR")
              .param("amount", "0.01"));
    }

    ExecutorService executorService =
        Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    CountDownLatch countDownLatch = new CountDownLatch(formList.size());
    Collections.shuffle(formList);

    formList.forEach(
        form -> {
          executorService.submit(
              () -> {
                Invocation.Builder builder =
                    target.path("transactions").request(MediaType.APPLICATION_JSON);
                Response response =
                    builder.post(
                        Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED), Response.class);
                String body = response.readEntity(String.class);
                countDownLatch.countDown();
              });
        });
    executorService.shutdown();
    countDownLatch.await();

    JSONObject account1Expected = new JSONObject();
    account1Expected.put("id", 1L);
    account1Expected.put("currency", Currency.EUR.name());
    account1Expected.put("balance", BigDecimal.valueOf(100.00));
    JSONObject account2Expected = new JSONObject();
    account2Expected.put("id", 2L);
    account2Expected.put("currency", Currency.EUR.name());
    account2Expected.put("balance", BigDecimal.valueOf(200.00));

    JSONAssert.assertEquals(account1Expected.toString(), getAccountBalance(1L), true);
    JSONAssert.assertEquals(account2Expected.toString(), getAccountBalance(2L), true);
  }

  private String getAccountBalance(long id) {
    Invocation.Builder builder = target.path("accounts/" + id).request(MediaType.APPLICATION_JSON);
    Response response = builder.get(Response.class);
    String body = response.readEntity(String.class);
    return body;
  }
}
